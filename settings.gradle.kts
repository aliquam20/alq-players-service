rootProject.name = "alq-players-service"

pluginManagement {
    repositories {
        mavenLocal()
        maven(url = "https://nexus.jeikobu.net/repository/maven-releases/")
        gradlePluginPortal()
    }
}
