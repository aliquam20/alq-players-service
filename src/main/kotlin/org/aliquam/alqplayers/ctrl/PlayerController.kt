package org.aliquam.alqplayers.ctrl

import org.aliquam.alqplayers.dao.Identifiers
import org.aliquam.alqplayers.dao.Player
import org.aliquam.alqplayers.dto.IdentifiersDto
import org.aliquam.alqplayers.dto.PlayerDto
import org.aliquam.alqplayers.exception.BadRequestException
import org.aliquam.alqplayers.exception.ResourceNotFoundException
import org.aliquam.alqplayers.mapper.IdentifiersMapper
import org.aliquam.alqplayers.mapper.PlayerMapper
import org.aliquam.alqplayers.repo.PlayerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class PlayerController {
    @Autowired
    lateinit var playerRepository: PlayerRepository
    @Autowired
    lateinit var playerMapper: PlayerMapper
    @Autowired
    lateinit var identifiersMapper: IdentifiersMapper

    @GetMapping("/alqplayers/player/{uuid}")
    fun getPlayer(@PathVariable uuid: UUID) = playerRepository.findByInternalID(uuid) ?: throw ResourceNotFoundException("Player not found!")

    @GetMapping("/alqplayers/identifiers")
    fun getIdentifiers(@RequestParam(name = "internal", required = false) internalID: UUID?,
                       @RequestParam(name = "minecraft", required = false) minecraftID: UUID?,
                       @RequestParam(name = "discord", required = false) discordID: Long?) : Identifiers {
        if (moreThanOneNonNull(internalID, minecraftID, discordID)) {
            throw BadRequestException("More than one ID supplied, check request")
        }

        val player = when {
            internalID != null -> {
                playerRepository.findByInternalID(internalID)
            }
            minecraftID != null -> {
                playerRepository.findByIdentifiersMinecraftID(minecraftID)
            }
            discordID != null -> {
                playerRepository.findByIdentifiersDiscordID(discordID)
            }
            else -> {
                throw BadRequestException("No IDs supplied, check request")
            }
        } ?: throw ResourceNotFoundException("Player (Internal: $internalID, MC: $minecraftID, Discord: $discordID) not found!")

        return player.identifiers
    }

    @PatchMapping("/alqplayers/identifiers")
    fun patchIdentifiers(@RequestParam(name = "internal", required = true) internalID: UUID,
                         @RequestBody identifiersDto: IdentifiersDto): Identifiers {
        val player = playerRepository.findByInternalID(internalID) ?: throw ResourceNotFoundException("Player not found!")
        identifiersMapper.updateIdentifiersFromDto(identifiersDto, player.identifiers)
        return playerRepository.save(player).identifiers
    }


    @PostMapping("/alqplayers/player")
    fun postPlayer(@RequestBody player: Player) = playerRepository.save(player)

    @PatchMapping("/alqplayers/player")
    fun patchPlayer(@RequestBody dto: PlayerDto): Player {
        val player = playerRepository.findByInternalID(dto.internalID) ?: throw ResourceNotFoundException("Player not found! $dto.internalID")
        playerMapper.updatePlayerFromDto(dto, player)
        return playerRepository.save(player)
    }

    fun moreThanOneNonNull(vararg objects: Any?) = objects.mapNotNull { it }.size > 1
}