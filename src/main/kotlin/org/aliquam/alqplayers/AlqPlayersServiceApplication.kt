package org.aliquam.alqplayers

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories
class AlqPlayersServiceApplication

fun main(args: Array<String>) {
    runApplication<AlqPlayersServiceApplication>(*args)
}
