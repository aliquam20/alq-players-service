package org.aliquam.alqplayers.repo

import org.aliquam.alqplayers.dao.Player
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PlayerRepository : CrudRepository<Player, UUID> {
    fun findByInternalID(uuid: UUID): Player?
    fun findAllByLastServer(uuid: UUID): List<Player?>
    fun findByIdentifiersMinecraftID(uuid: UUID): Player?
    fun findByIdentifiersDiscordID(id: Long): Player?
}

