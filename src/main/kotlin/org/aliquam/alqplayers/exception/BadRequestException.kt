package org.aliquam.alqplayers.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
class BadRequestException(message: String = "Bad request!") : RuntimeException(message)