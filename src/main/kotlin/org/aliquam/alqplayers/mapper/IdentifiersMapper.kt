package org.aliquam.alqplayers.mapper

import org.aliquam.alqplayers.dao.Identifiers
import org.aliquam.alqplayers.dto.IdentifiersDto
import org.mapstruct.BeanMapping
import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.mapstruct.NullValuePropertyMappingStrategy

@Mapper(componentModel = "spring")
interface IdentifiersMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    fun updateIdentifiersFromDto(dto: IdentifiersDto, @MappingTarget entity: Identifiers)
}
