package org.aliquam.alqplayers.mapper

import org.aliquam.alqplayers.dao.Player
import org.aliquam.alqplayers.dto.PlayerDto
import org.mapstruct.BeanMapping
import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.mapstruct.NullValuePropertyMappingStrategy

@Mapper(componentModel = "spring")
interface PlayerMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    fun updatePlayerFromDto(dto: PlayerDto, @MappingTarget entity: Player)
}