package org.aliquam.alqplayers.dto

import java.time.LocalDateTime
import java.util.*

data class PlayerDto(val internalID: UUID,
                     var lastOnline: LocalDateTime? = null,
                     var lastName: String? = null,
                     var lastServer: UUID? = null,
                     var lastWorld: UUID? = null,
                     var balance: Double? = null,
                     var timezone: TimeZone? = null,
                     var language: String? = null
)