package org.aliquam.alqplayers.dto

import java.util.*

data class IdentifiersDto(
        var internalID: UUID?,
        var minecraftID: UUID?,
        var discordID: Long?
)