package org.aliquam.alqplayers.dao

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Cascade
import org.hibernate.annotations.CascadeType
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
class Player(
    @Id val internalID: UUID = UUID.randomUUID(),
    @Version val version: Long? = null,
    var lastOnline: LocalDateTime = LocalDateTime.now(),
    var lastName: String = "nil",
    var lastServer: UUID = UUID.randomUUID(),
    var lastWorld: UUID = UUID.randomUUID(),
    var balance: Double = 0.0,
    var timezone: TimeZone = TimeZone.getDefault(),
    var language: String = "EN",
    @JsonIgnore @OneToOne @PrimaryKeyJoinColumn @Cascade(CascadeType.ALL) val identifiers: Identifiers = Identifiers(internalID, null, null)
) : Serializable {

    override fun equals(other: Any?) = when {
        this === other -> true
        javaClass != other?.javaClass -> false
        internalID != (other as Player).internalID -> false
        else -> true
    }

    override fun hashCode(): Int = internalID.hashCode()
}