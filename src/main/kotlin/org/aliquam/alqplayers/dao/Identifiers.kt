package org.aliquam.alqplayers.dao

import java.util.*
import javax.persistence.*

@Entity
class Identifiers (
        @Id val internalID: UUID,
        var minecraftID: UUID?,
        var discordID: Long?
)