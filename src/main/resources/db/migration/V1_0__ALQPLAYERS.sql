create table public.identifiers
(
    internalid  uuid not null
        constraint identifiers_pkey
            primary key,
    discordid   bigint,
    minecraftid uuid
);

create table public.player
(
    internal_id uuid             not null
        constraint player_pkey
            primary key,
    balance     double precision not null,
    language    varchar(255),
    last_name   varchar(255),
    last_online timestamp,
    last_server uuid,
    last_world  uuid,
    timezone    varchar(255),
    version     bigint
);