package org.aliquam.alqplayers.ctrl

import com.fasterxml.jackson.databind.ObjectMapper
import org.aliquam.alqplayers.dao.Identifiers
import org.aliquam.alqplayers.dao.Player
import org.aliquam.alqplayers.dto.PlayerDto
import org.aliquam.alqplayers.mapper.PlayerMapper
import org.aliquam.alqplayers.mapper.PlayerMapperImpl
import org.aliquam.alqplayers.repo.PlayerRepository
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.BeforeEach
import org.mockito.ArgumentMatchers.argThat
import org.mockito.BDDMockito.*
import org.mockito.Mockito.verify
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import java.util.*
import org.junit.jupiter.api.Assertions.*
import org.mockito.internal.matchers.apachecommons.ReflectionEquals

@WebMvcTest(PlayerController::class)
@ComponentScan(basePackageClasses = [PlayerControllerTest::class, PlayerMapper::class, PlayerMapperImpl::class])
internal class PlayerControllerTest {
    @Autowired
    lateinit var mvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @MockBean
    lateinit var repo: PlayerRepository

    private val testUUID: UUID = UUID.randomUUID()
    private val testMinecraftUUID: UUID = UUID.randomUUID()
    private val testDiscordID: Long = 111903445539139584
    private val testDTO = PlayerDto(testUUID, lastName = "JonProton")

    private lateinit var testPlayer: Player
    private lateinit var modifiedTestPlayer: Player
    private lateinit var testIdentifiers: Identifiers

    @BeforeEach
    fun initializer() {
        testIdentifiers = Identifiers(testUUID, testMinecraftUUID, testDiscordID)
        testPlayer = Player(testUUID)
        modifiedTestPlayer = Player(testUUID, lastName = "JonProton",
                lastOnline = testPlayer.lastOnline,
                lastWorld = testPlayer.lastWorld,
                lastServer = testPlayer.lastServer,
                identifiers = testIdentifiers)
    }

    @Test
    fun getPlayersTest() {
        given(repo.findByInternalID(testUUID)).willReturn(testPlayer)

        mvc.perform(get("/alqplayers/player/${testUUID}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.internal-id", `is`(testPlayer.internalID.toString())))
                .andExpect(jsonPath("$.last-name", `is`(testPlayer.lastName)))
    }

    @Test
    fun getIdentifiersByInternalID() {
        given(repo.findByInternalID(testUUID)).willReturn(modifiedTestPlayer)

        mvc.perform(get("/alqplayers/identifiers?internal=${testUUID}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.internal-id", `is`(testUUID.toString())))
                .andExpect(jsonPath("$.discord-id", `is`(testDiscordID)))
                .andExpect(jsonPath("$.minecraft-id", `is`(testMinecraftUUID.toString())))
    }

    @Test
    fun getIdentifiersByMinecraftID() {
        given(repo.findByIdentifiersMinecraftID(testMinecraftUUID)).willReturn(modifiedTestPlayer)

        mvc.perform(get("/alqplayers/identifiers?minecraft=${testMinecraftUUID}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.internal-id", `is`(testUUID.toString())))
                .andExpect(jsonPath("$.discord-id", `is`(testDiscordID)))
                .andExpect(jsonPath("$.minecraft-id", `is`(testMinecraftUUID.toString())))
    }

    @Test
    fun getIdentifiersByDiscordID() {
        given(repo.findByIdentifiersDiscordID(testDiscordID)).willReturn(modifiedTestPlayer)

        mvc.perform(get("/alqplayers/identifiers?discord=${testDiscordID}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.internal-id", `is`(testUUID.toString())))
                .andExpect(jsonPath("$.discord-id", `is`(testDiscordID)))
                .andExpect(jsonPath("$.minecraft-id", `is`(testMinecraftUUID.toString())))
    }

    @Test
    fun postPlayerTest() {
        `when`(repo.save(any(Player::class.java))).thenAnswer { it.arguments[0] }

        mvc.perform(post("/alqplayers/player/").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(testPlayer)))
                .andExpect(status().isOk)

        verify(repo).save(argThat {
            ReflectionEquals(testPlayer, "identifiers").matches(it) })
    }

    @Test
    fun patchPlayerTest() {
        given(repo.findByInternalID(testUUID)).willReturn(testPlayer)
        `when`(repo.save(any(Player::class.java))).thenAnswer { it.arguments[0] }

        mvc.perform(patch("/alqplayers/player")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testDTO)))
                .andExpect(status().isOk)

        verify(repo).save(argThat { ReflectionEquals(modifiedTestPlayer, "identifiers").matches(it) })
    }
}