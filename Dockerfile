FROM adoptopenjdk/openjdk11:alpine-jre
EXPOSE 4100
ADD /build/libs/*.jar alq-players-service.jar
ENTRYPOINT ["java", "-jar", "alq-players-service.jar"]