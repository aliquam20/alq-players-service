import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootBuildImage
import org.aliquam.AlqUtils

plugins {
    id("org.springframework.boot") version "2.4.0"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    id("org.sonarqube") version "3.0"
    jacoco
    war
    kotlin("plugin.allopen") version "1.4.10"
    kotlin("jvm") version "1.4.10"
    kotlin("plugin.spring") version "1.4.10"
    kotlin("plugin.jpa") version "1.4.10"
    kotlin("kapt") version "1.4.10"
    id("org.aliquam.alq-gradle-parent") version "0.4.14"
}
val alq = AlqUtils(project)

val baseVersion = "1.0.0"
val branchName: String? = System.getenv("BRANCH_NAME")
val buildNumber: String? = System.getenv("BUILD_NUMBER")

group = "org.aliquam"
version = alq.getSemVersion(baseVersion)
java.sourceCompatibility = JavaVersion.VERSION_11

allOpen {
    annotation("javax.persistence.Entity")
    annotation("javax.persistence.Embeddable")
    annotation("javax.persistence.MappedSuperclass")
}

sonarqube {
    if (branchName != null) {
        properties {
            property("sonar.projectKey", "aliquam20_alq-players-service")
            property("sonar.organization", "aliquam")
            property("sonar.host.url", "https://sonarcloud.io")
            property("sonar.branch.name", branchName)
            property("sonar.coverage.jacoco.xmlReportPaths", "$projectDir/build/reports/jacoco/test/jacocoTestReport.xml")
        }
    }
}

tasks.getByName<BootBuildImage>("bootBuildImage") {
    when (branchName) {
        "master" -> {
            imageName = "docker.jeikobu.net/aliquam/alq-players-service:latest"
            isPublish = true
        }
        "develop" -> {
            imageName = "docker.jeikobu.net/aliquam/alq-players-service:snapshot"
            isPublish = true
        }
        else -> {
            imageName = "aliquam/alq-players-service:dev_build"
            isPublish = false
        }
    }

    if (branchName != null) {
        docker {
            publishRegistry {
                username = System.getenv("DOCKER_REGISTRY_USER")
                password = System.getenv("DOCKER_REGISTRY_PASS")
                url = "https://docker.jeikobu.net/v2/"
            }
        }
    }
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootJar>("bootJar") {
    if (branchName == null) {
        classpath(configurations["developmentOnly"])
    }
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        csv.isEnabled = false
    }
    dependsOn(tasks.test) // tests are required to run before generating the report
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.mapstruct:mapstruct:1.4.1.Final")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.kafka:spring-kafka")
    implementation("org.flywaydb:flyway-core:6.5.7")
    implementation("org.springdoc:springdoc-openapi-ui:1.5.0")

    developmentOnly("org.springframework.boot:spring-boot-devtools")
    developmentOnly("org.springframework.boot:spring-boot-starter-actuator")

    runtimeOnly("org.postgresql:postgresql")
    providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("org.springframework.kafka:spring-kafka-test")
    testImplementation("org.flywaydb.flyway-test-extensions:flyway-spring-test:6.4.0")
    testRuntimeOnly("com.h2database:h2:1.4.199")

    kapt("org.mapstruct:mapstruct-processor:1.4.1.Final")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
